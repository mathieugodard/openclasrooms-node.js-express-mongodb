[[_TOC_]]

# Le projet

Réalisation du backend du cours OpenClassRooms intitulé "[Passez au Full Stack avec Node.js, Express et MongoDB](https://openclassrooms.com/fr/courses/6390246-passez-au-full-stack-avec-node-js-express-et-mongodb)".

Vous pouvez consulter ma [prise de notes](https://framagit.org/mathieugodard/mes-notes/-/blob/main/OCR%20-%20Nodejs%20Expressjs%20Mongodb.md).

# Les commandes

## Au préalable

Installer Nodejs : https://nodejs.org/en/

A titre personnel (Debian) :
```bash
$ sudo curl -fsSL https://deb.nodesource.com/setup_16.x | bash -
apt-get install -y nodejs
```

## Frontend

Github du projet : 
https://github.com/OpenClassrooms-Student-Center/go-fullstack-v3-fr

Commandes pour lancer le frontend sur un serveur de développement :

```bash
$ git clone https://github.com/OpenClassrooms-Student-Center/go-fullstack-v3-fr.git frontend
$ cd frontend
$ npm install
$ npm run start
```

## Backend

Une fois le frontend lancé, le code du backend correspond à la fin du cours, donc la partie 4 du projet.

Commandes pour lancer le serveur de développement :

```bash
$ git clone git@framagit.org:mathieugodard/openclasrooms-node.js-express-mongodb.git backend
$ cd backend
$ npm install
$ nodemon server
```